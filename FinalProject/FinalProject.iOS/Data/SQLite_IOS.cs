﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using FinalProject.iOS.Data;
using FinalProject.Models;
using Foundation;
using System.IO;
using Xamarin.Forms;
using UIKit;
using SQLite;

[assembly: Xamarin.Forms.Dependency(typeof(SQLite_IOS))]
namespace FinalProject.iOS.Data
{
    public class SQLite_IOS : ISQLite

    {
        public SQLite_IOS()
        { }
        public SQLite.SQLiteConnection GetConnection()
        {
            var filename = "CSUSM.db3";
            var documentpath = Environment.GetFolderPath(Environment.SpecialFolder.Personal);
            var librarypath = Path.Combine(documentpath, "..", "Library");
            var path = Path.Combine(librarypath, filename);
            var connection = new SQLite.SQLiteConnection(path);
            return connection;

        }
    }
}