﻿using SQLite;
using System.IO;
using FinalProject.Models;
using FinalProject.Droid.Data;

[assembly: Xamarin.Forms.Dependency(typeof(SQLite_Android))]
namespace FinalProject.Droid.Data
{
        public class SQLite_Android : ISQLite
        {
            public SQLiteConnection GetConnection()
            {
                string sqliteFileName = "CSUSM7.db3";

                string documentPath = System.Environment.GetFolderPath(System.Environment.SpecialFolder.Personal);

                string path = Path.Combine(documentPath, sqliteFileName);

                var conn = new SQLiteConnection(path);

                return conn;
            }
        }
 
}