﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Android.Net;
using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using FinalProject.Models;
using FinalProject.Droid.Data;

[assembly: Xamarin.Forms.Dependency(typeof(Networkconnection))]
namespace FinalProject.Droid.Data
{
    public class Networkconnection : INetworkconnection
    {
        public bool IsConnected { get; set; }

        public void checkNetworkConnection()
        {
            var  ConnectivityManager = (ConnectivityManager)Application.Context.GetSystemService(Context.ConnectivityService);
            var ActiveNetworkInfo = ConnectivityManager.ActiveNetworkInfo;
            if(ActiveNetworkInfo != null && ActiveNetworkInfo.IsConnected)
            {
                IsConnected = true;
            }
            else
            {
                IsConnected = false;
            }
        }
    }
}