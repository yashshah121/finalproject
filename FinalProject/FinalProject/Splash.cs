﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection.Emit;
using System.Text;
using Xamarin.Forms;

namespace FinalProject
{
    //Class for having Splash while the App is loading
   public class Splash : ContentPage
    {
        Image splashlogo;
        public Splash()
        {
            NavigationPage.SetHasNavigationBar(this, false);
            //Defining Layout
            var sub = new AbsoluteLayout();
            //getting the logo for the page
            splashlogo = new Image
            {
                Source = "csusm.png",
                WidthRequest = 300,
                HeightRequest = 300
            };
            //setting up the parameters.
            AbsoluteLayout.SetLayoutFlags(splashlogo,
               AbsoluteLayoutFlags.PositionProportional);
            AbsoluteLayout.SetLayoutBounds(splashlogo,
             new Rectangle(0.5, 0.5, AbsoluteLayout.AutoSize, AbsoluteLayout.AutoSize));
            //adding logo
            sub.Children.Add(splashlogo);

           
            this.Content = sub;
        }
        protected override async void OnAppearing()
        {
            //AddingWhen App starts
            base.OnAppearing();

            await splashlogo.ScaleTo(1, 2000); //Time-consuming processes such as initialization
           //Directing it to login page.
            Application.Current.MainPage = new NavigationPage(new LoginPage());    //After loading  MainPage it gets Navigated to our new Page
        }
    }
}
