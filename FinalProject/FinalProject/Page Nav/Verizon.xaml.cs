﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace FinalProject
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class Verizon : ContentPage
    {
        public Verizon()
        {
            InitializeComponent();

        }
        
             protected async override void OnAppearing()
        {
            base.OnAppearing();
            var browser = new WebView();
            browser.Source = "https://www.verizon.com/solutions-and-services/international-student/";
            Content = browser;

        }
    }
}