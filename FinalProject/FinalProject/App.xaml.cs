﻿using System;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;
//using FinalProject.Services;
using FinalProject.Views;
using FinalProject.Models;
using System.Transactions;
using System.Threading;
using System.Threading.Tasks;

namespace FinalProject
{
    public partial class App : Application
    {
        //Calling token controller, Database Controller and Rest service an dcheking it with methods down.
        private static TokenDatabaseController tokenDatabase;
        private static UserDatabaseController userDatabase;
        private static RestService restService;
        //Label for internet display.
        public static Label labeldisplay;
        private static bool hasInternet;

        public static Page Currentpage;
        //Timer for Cechking Internet connection after few intervals
        private static Timer timer;
        //if no internet then function which will be performed.
        private static bool noInterShow;

        public App()
        {
            InitializeComponent();
            

            MainPage = new Splash();
            //MainPage = new NavigationPage(new MainPage());
        }

        protected override void OnStart()
        {
        }

        protected override void OnSleep()
        {
        }

        protected override void OnResume()
        {
        }
        public static UserDatabaseController UserDatabase
        {
            get
            {
                if (userDatabase == null)
                {
                    userDatabase = new UserDatabaseController();
                }

                return userDatabase;
            }
        }

        public static TokenDatabaseController TokenDatabase
        {
            get
            {
                if (tokenDatabase == null)
                {
                    tokenDatabase = new TokenDatabaseController();
                }

                return tokenDatabase;
            }
        }

        public static RestService RestService
        {
            get
            {
                if (restService == null)
                {
                    restService = new RestService();
                }

                return restService;
            }
        }
        //checking Internated Connection
        public static void startcheckinternet(Label label, Page page)
        {
            labeldisplay = label;
            label.Text = Constants.NotInternetText;
            label.IsVisible = false;
            hasInternet = true;
            Currentpage = page;
            if(timer == null)
            {
                timer = new Timer((e) =>
                {
                    CheckIfInternetOverTime();
                }, null, 10, (int)TimeSpan.FromSeconds(3).TotalMilliseconds);
            }
        }
        private static void CheckIfInternetOverTime()
        {
            INetworkconnection networkConnection = DependencyService.Get<INetworkconnection>();
            networkConnection.checkNetworkConnection();
            if (!networkConnection.IsConnected)
            {
                Device.BeginInvokeOnMainThread(async () =>
                {
                    if (hasInternet)
                    {
                        if (!noInterShow)
                        {
                            hasInternet = false;
                            labeldisplay.IsVisible = true;
                            await ShowDisplayAlert();
                        }
                    }
                });
            }
            else
            {
                Device.BeginInvokeOnMainThread(() =>
                {
                    hasInternet = true;
                    labeldisplay.IsVisible = false;
                });
            }
        }
        public static async Task<bool> CheckIfInternet()
        {
            INetworkconnection networkConnection = DependencyService.Get<INetworkconnection>();
            networkConnection.checkNetworkConnection();

            return networkConnection.IsConnected;
        }

        public static async Task<bool> CheckIfInternetAlert()
        {
            INetworkconnection networkConnection = DependencyService.Get<INetworkconnection>();
            networkConnection.checkNetworkConnection();

            if (!networkConnection.IsConnected)
            {
                if (!noInterShow)
                {
                    await ShowDisplayAlert();
                }

                return false;
            }

            return true;
        }

        private static async Task ShowDisplayAlert()
        {
            noInterShow = false;
            await Currentpage.DisplayAlert("Internet", "Device has no internet, please reconnect", "Ok");
            noInterShow = false;
        }
    }
}
