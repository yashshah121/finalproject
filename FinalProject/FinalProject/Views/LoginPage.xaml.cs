﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace FinalProject
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class LoginPage : ContentPage
    {
        public LoginPage()
        {
            InitializeComponent();
            Init();
        }
        void Init()
        {
            BackgroundColor = Constants.BackgroundColor;
            UserName_Label.TextColor = Constants.MainTextColor;
            Password_Labe2.TextColor = Constants.MainTextColor;
            ActivitySpinner.IsVisible = false;

            Login_Logo.HeightRequest = Constants.Login_LogoHeight;
            App.startcheckinternet(NoInternet_Label, this);
            UserName_input.Completed += (s, e) => Password_Input.Focus();
            Password_Input.Completed += (s, e) => SiginClicked(s, e);


        }
        async void SiginClicked(Object sender, EventArgs e)
        {
            //getting data from field.
            UserData userdata = new UserData(UserName_input.Text, Password_Input.Text);
            if (userdata.CheckInfo())
            {
                TokenDetails token = await App.RestService.Login(userdata);
                //accessing token id
                if (token.access_tokenid != null )
                {
                    //saving user and token
                    App.UserDatabase.SaveUser(userdata);
                    App.TokenDatabase.SaveToken(token);

                    

                    if (Device.OS == TargetPlatform.Android)
                    {
                        //diverting to mainpage
                        Application.Current.MainPage = new NavigationPage(new MainPage());
                    }
                    else  
                    {
                        await Navigation.PushModalAsync(new NavigationPage(new MainPage()));
                    }
                   
                }
            }
            else
            {
                await DisplayAlert("Login", "Login Not Correct, empty username or password.", "Ok");
            }
        }
    }
}