﻿using Xamarin.Forms;
using Xamarin.Forms.Maps;
using Xamarin.Forms.Xaml;
using System.Collections.ObjectModel;

namespace FinalProject
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class GroceryMap : ContentPage
    {
        ObservableCollection<Pin> pinNames;

        public GroceryMap()
        {
            InitializeComponent();
            MainMap.MapType = MapType.Street;

            ////Sets the initial map load location with the radies of 10 miles.
            var initialMapLocation = MapSpan.FromCenterAndRadius
                                                (new Position(33.1305353, -117.1644996)
                                                 , Distance.FromMiles(3));

            MainMap.MoveToRegion(initialMapLocation);
            //calling picker function
            PopulatePicker();

        }
        private void PopulatePicker()
        {
            //Adding 3 pins in the list
            pinNames = new ObservableCollection<Pin>
            {
               new Pin
                {

                Label = "Costco",
                Address = "725 Center Dr, San Marcos, CA 92069",
                Type = PinType.SavedPin,
                Position = new Position(33.1365314,-117.1395308)

                },
                new Pin
                {
                Position = new Position(33.1303505,-117.1664517),
                Label = "Ralph",
                Address = "306 S Twin Oaks Valley Rd, San Marcos, CA 92078",
                Type = PinType.SavedPin
                },
                new Pin
                {
                Position = new Position(33.137668,-117.1815197),
                Label = "Winco",
                Address = "555 Grand Ave, San Marcos, CA 92078",
                Type = PinType.SavedPin
                },
                 new Pin
                {
                Position = new Position(33.1293577,-117.1661748),
                Label = "CVS",
                Address = "320 S Twin Oaks Valley Rd, San Marcos, CA 92078",
                Type = PinType.SavedPin
                }

            };
            //Adding it to picker
            picker.Items.Add("Select which Mobile store you want to Visit");
            foreach (Pin item in pinNames)
            {
                //Load each pin into the map and picker
                picker.Items.Add(item.Label);
                MainMap.Pins.Add(item);
            }
            //setting index to 0.
            picker.SelectedIndex = 0;
            picker.SelectedIndexChanged += (sender, e) =>
            {
                var picker = (Picker)sender;
                string pname = picker.SelectedItem.ToString();

                foreach (Pin item in pinNames)
                {
                    if (item.Label == pname)
                    {
                        //Move the map to center on the pin
                        MainMap.MoveToRegion(MapSpan.FromCenterAndRadius(item.Position, Distance.FromMiles(1)).WithZoom(19));
                    }
                }
            };

        }



    }
}
