﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace FinalProject
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class Jobs : ContentPage
    {
        public Jobs()
        {
            InitializeComponent();
        }
        [Obsolete]
        private void TapGestureRecognizer_HandShake(object sender, EventArgs e)
        {
           // Device.OpenUri(new Uri("https://csusm.joinhandshake.com/")); //Open the site in browser
            Device.OpenUri(new Uri("https://csusm.joinhandshake.com"));


        }
    }
}