﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using Xamarin.Forms;
using Xamarin.Forms.Maps;
using Xamarin.Forms.Xaml;
using System.Collections.ObjectModel;
namespace FinalProject
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class MobileMap : ContentPage
    {
        ObservableCollection<Pin> pinNames;

        public MobileMap()
        {
            InitializeComponent();
            MainMap.MapType = MapType.Street;

            ////Sets the initial map load location with the radies of 10 miles.
            var initialMapLocation = MapSpan.FromCenterAndRadius
                                                (new Position(33.1318586, -117.1808915)
                                                 , Distance.FromMiles(3));

            MainMap.MoveToRegion(initialMapLocation);
            //calling picker function
            PopulatePicker();

        }
        private void PopulatePicker()
        {
            //Adding 3 pins in the list
            pinNames = new ObservableCollection<Pin>
            {
               new Pin
                {

                Label = "Tmobile",
                Address = "595 Grand Ave #100, San Marcos, CA 92078",
                Type = PinType.SavedPin,
                Position = new Position(33.1341748,-117.177765)

                },
                new Pin
                {
                Position = new Position(33.1402453,-117.1926179),
                Label = "Verizon",
                Address = "151 S Las Posas Rd #175, San Marcos, CA 92078",
                Type = PinType.SavedPin
                },
                new Pin
                {
                Position = new Position(33.1402443,-117.199184),
                Label = "Tmobile",
                Address = "595 Grand Ave #100, San Marcos, CA 92078",
                Type = PinType.SavedPin
                }

            };
            //Adding it to picker
            picker.Items.Add("Select whichStore you want to Visit");
            foreach (Pin item in pinNames)
            {
                //Load each pin into the map and picker
                picker.Items.Add(item.Label);
                MainMap.Pins.Add(item);
            }
            //setting index to 0.
            picker.SelectedIndex = 0;
            picker.SelectedIndexChanged += (sender, e) =>
            {
                var picker = (Picker)sender;
                string pname = picker.SelectedItem.ToString();

                foreach (Pin item in pinNames)
                {
                    if (item.Label == pname)
                    {
                        //Move the map to center on the pin
                        MainMap.MoveToRegion(MapSpan.FromCenterAndRadius(item.Position, Distance.FromMiles(1)).WithZoom(19));
                    }
                }
            };

        }
    }
}