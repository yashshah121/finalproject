﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Essentials;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace FinalProject
{//Hoempage with all click events.
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class HomePage : ContentPage
    {
        public HomePage()
        {
            InitializeComponent();
        }
        async void OnATTClicked(object sender, EventArgs args)
        {
            await Navigation.PushAsync(new Att());

        }
        async void OnTmobileClicked(object sender, EventArgs args)
        {

            await Navigation.PushAsync(new TMobile());
        }
        async void OnVerizonClicked(object sender, EventArgs args)
        {
            await Navigation.PushAsync(new Verizon());
        }
        public async void OnLocationClicked(object sender, EventArgs e)
        {
            await Navigation.PushAsync(new MapPage());
        }
        async void OnBofaClicked(object sender, EventArgs args)
        {
            await Navigation.PushAsync(new Bofa());
        }
        async void OnChaseClicked(object sender, EventArgs args)
        {
            await Navigation.PushAsync(new Chase());
        }
        public async void OnMobileLocationClicked(object sender, EventArgs e)
        {
            await Navigation.PushAsync(new MobileMap());
        }
        public async void OnCsusmMTSClicked(object sender, EventArgs e)
        {
            await Navigation.PushAsync(new MTS());
        }
    }
}