﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace FinalProject
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class Shopping : ContentPage
    {
        public Shopping()
        {
            InitializeComponent();
        }

        [Obsolete]
        //opening Ralphs when uri Clicked.

        private void TapGestureRecognizer_Ralph(object sender, EventArgs e)
        {
            Device.OpenUri(new Uri("https://www.ralphs.com/weeklyad")); //Open the site in browser


        }

        [Obsolete]
        private void TapGestureRecognizer_Costco(object sender, EventArgs e)
        {
            Device.OpenUri(new Uri("https://www.costco.com/join-costco.html")); //Open the site in browser



        }

        [Obsolete]
        public void TapGestureRecognizer_Winco(object sender, EventArgs e)
        {
            Device.OpenUri(new Uri("https://www.wincofoods.com/home#")); //Open the site in browser


        }

        [Obsolete]
        private void TapGestureRecognizer_CVS(object sender, EventArgs e)
        {
            Device.OpenUri(new Uri("https://www.cvs.com/")); //Open the site in browser


        }
       async void TapGestureRecognizer_Location(object sender, EventArgs e)
        {
            await Navigation.PushAsync(new GroceryMap());


        }

    }
}