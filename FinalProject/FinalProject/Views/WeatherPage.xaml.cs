﻿using FinalProject.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace FinalProject
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class WeatherPage : ContentPage
    {// creating weather service object
        WeatherService _weatherService;

        public WeatherPage()
        {
            InitializeComponent();
            _weatherService = new WeatherService();

        }
        async void OnGetWeatherButtonClicked(object sender, EventArgs e)
        {
            //getting data using uri
            if (!string.IsNullOrWhiteSpace(_cityEntry.Text))
            {
                GetData weatherData = await _weatherService.GetWeatherData(GenerateRequestUri(URIAPI.OpenWeatherMapEndpoint));
                BindingContext = weatherData;
            }
        }

        string GenerateRequestUri(string endpoint)
        {
            string requestUri = endpoint;
            requestUri += $"?q={_cityEntry.Text}";
            requestUri += "&units=imperial"; // or units=metric
            requestUri += $"&APPID={URIAPI.OpenWeatherMapAPIKey}";
            return requestUri;
        }
    }
}