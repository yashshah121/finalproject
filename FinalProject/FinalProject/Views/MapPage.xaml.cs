﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.Diagnostics;


using Xamarin.Forms;
using Xamarin.Forms.Maps;
using Xamarin.Forms.Xaml;
using System.Collections.ObjectModel;

namespace FinalProject
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class MapPage : ContentPage
    {
        ObservableCollection<Pin> pinNames;

        public MapPage()
        {
            InitializeComponent();
            MainMap.MapType = MapType.Street;

            ////Sets the initial map load location with the radies of 10 miles.
            var initialMapLocation = MapSpan.FromCenterAndRadius
                                                (new Position(33.1318586, -117.1808915)
                                                 , Distance.FromMiles(2));

            MainMap.MoveToRegion(initialMapLocation);
            //calling picker function
            PopulatePicker();

        }
        private void PopulatePicker()
        {
            //Adding 3 pins in the list
            pinNames = new ObservableCollection<Pin>
            {
               new Pin
                {

                Label = "Chase Bank",
                Address = "348 S Twin Oaks Valley Rd Ste 155, San Marcos, CA 92078",
                Type = PinType.SavedPin,
                Position = new Position(33.1315528,-117.1664242)

                },
                new Pin
                {
                Position = new Position(33.1308183,-117.1659628),
                Label = "Bank Of America",
                Address = "336 S Twin Oaks Valley Rd, San Marcos, CA 92078",
                Type = PinType.SavedPin
                },
                new Pin
                {
                Position = new Position(33.1355233,-117.1936703),
                Label = "Wells Fargo",
                Address = "1000 W San Marcos Blvd, San Marcos, CA 92078",
                Type = PinType.SavedPin
                },
               
            };
            //Adding it to picker
            picker.Items.Add("Select which Bank you want to Visit");
            foreach (Pin item in pinNames)
            {
                //Load each pin into the map and picker
                picker.Items.Add(item.Label);
                MainMap.Pins.Add(item);
            }
            //setting index to 0.
            picker.SelectedIndex = 0;
            picker.SelectedIndexChanged += (sender, e) =>
            {
                var picker = (Picker)sender;
                string pname = picker.SelectedItem.ToString();

                foreach (Pin item in pinNames)
                {
                    if (item.Label == pname)
                    {
                        //Move the map to center on the pin
                        MainMap.MoveToRegion(MapSpan.FromCenterAndRadius(item.Position, Distance.FromMiles(1)).WithZoom(19));
                    }
                }
            };

        }



    }
}