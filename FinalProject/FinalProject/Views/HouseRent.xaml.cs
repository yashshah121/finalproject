﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace FinalProject
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class HouseRent : ContentPage
    {
        public HouseRent()
        {
            InitializeComponent();
        }
        async void OnBarhamClicked(object sender, EventArgs args)
        {
            await Navigation.PushAsync(new Barham());

        }
        async void OnCrestClicked(object sender, EventArgs args)
        {
            await Navigation.PushAsync(new Crest());

        }
        async void OnProminanceClicked(object sender, EventArgs args)
        {
            await Navigation.PushAsync(new Prominance());

        }
        async void OnUVAClicked(object sender, EventArgs args)
        {
            await Navigation.PushAsync(new UVA());

        }
    }
}