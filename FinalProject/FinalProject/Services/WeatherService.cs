﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace FinalProject.Services
{
    //Creating httpp client and weather service data.
    public class WeatherService
    {
        HttpClient _client;
        public WeatherService()
        {
            _client = new HttpClient();
        }
        public async Task<GetData> GetWeatherData(string query)
        {
            GetData weatherData = null;
            try
            {
                var response = await _client.GetAsync(query);
                if (response.IsSuccessStatusCode)
                {
                    var content = await response.Content.ReadAsStringAsync();
                    weatherData = JsonConvert.DeserializeObject<GetData>(content);
                }
            }
            catch (Exception ex)
            {
                Debug.WriteLine("\t\tERROR {0}", ex.Message);
            }

            return weatherData;
        }

    }
}
