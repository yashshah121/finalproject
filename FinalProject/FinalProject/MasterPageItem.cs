﻿using System;
using System.Collections.Generic;
using System.Text;

namespace FinalProject
{
    class MasterPageItem
    {
        //For master detail page.
        public string Title { get; set; }

        public string IconSource { get; set; }

        public Type TargetType { get; set; }
    }
}
