﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using FinalProject.Models;
using FinalProject;

namespace FinalProject.Models
{
    public class RestService
    {
        private HttpClient _httpClient;

        private string grant_type = "password";

        public RestService()
        {
            _httpClient = new HttpClient();
            _httpClient.MaxResponseContentBufferSize = 256000;
            _httpClient.DefaultRequestHeaders.Accept.Add(new System.Net.Http.Headers.MediaTypeWithQualityHeaderValue("application/x-www-form-urlenconded"));
        }

        public async Task<TokenDetails> Login(UserData userdata)
        {
            List<KeyValuePair<string, string>> postData = new List<KeyValuePair<string, string>>();
            postData.Add(new KeyValuePair<string, string>("grant_type", grant_type));
            postData.Add(new KeyValuePair<string, string>("username", userdata.Username));
            postData.Add(new KeyValuePair<string, string>("password", userdata.Password));

            FormUrlEncodedContent content = new FormUrlEncodedContent(postData);
            string weburl = "www.test.com";

            // Token token = await PostResponseLogin<Token>(weburl, content); this will work when we will have an actual server to call
            TokenDetails token = new TokenDetails() { access_tokenid = "test", expires = 30 };
            token.expire_dateinfo = DateTime.Now.AddMinutes(token.expires);

            return token;
        }

        public async Task<T> PostResponseLogin<T>(string weburl, FormUrlEncodedContent content) where T : class
        {
            HttpResponseMessage httpResponseMessage = await _httpClient.PostAsync(weburl, content);

            string jsonResult = await httpResponseMessage.Content.ReadAsStringAsync();

            return JsonConvert.DeserializeObject<T>(jsonResult);
        }

        public async Task<T> PostResponse<T>(string weburl, string jsonstring) where T : class
        {
            TokenDetails token = App.TokenDatabase.GetToken();

            _httpClient.DefaultRequestHeaders.Authorization = new System.Net.Http.Headers.AuthenticationHeaderValue("Berear", token.access_tokenid);

            try
            {
                HttpResponseMessage httpResponseMessage = await _httpClient.PostAsync(weburl, new StringContent(jsonstring, Encoding.UTF8, "application/json"));

                if (httpResponseMessage.StatusCode == System.Net.HttpStatusCode.OK)
                {
                    string jsonResult = await httpResponseMessage.Content.ReadAsStringAsync();

                    return JsonConvert.DeserializeObject<T>(jsonResult);
                }
            }
            catch (Exception e)
            {

            }

            return null;
        }

        public async Task<T> GetResponse<T>(string weburl) where T : class
        {
            TokenDetails token = App.TokenDatabase.GetToken();

            _httpClient.DefaultRequestHeaders.Authorization = new System.Net.Http.Headers.AuthenticationHeaderValue("Berear", token.access_tokenid);

            try
            {
                HttpResponseMessage httpResponseMessage = await _httpClient.GetAsync(weburl);

                if (httpResponseMessage.StatusCode == System.Net.HttpStatusCode.OK)
                {
                    string jsonResult = await httpResponseMessage.Content.ReadAsStringAsync();

                    return JsonConvert.DeserializeObject<T>(jsonResult);
                }
            }
            catch (Exception e)
            {

            }

            return null;
        }
    }
}