﻿using SQLite;
using System;
using System.Collections.Generic;
using System.Text;

namespace FinalProject
{
    public class TokenDetails
    {
        [PrimaryKey, AutoIncrement, SQLite.Column("Id")]
        //  [PrimaryKey]
        public int Id { get; set; }
        public string access_tokenid { get; set; }
        public string error_info { get; set; }

        public DateTime expire_dateinfo { get; set; }
        

        public int expires { get; set; }
        public TokenDetails() { }

    }
}
