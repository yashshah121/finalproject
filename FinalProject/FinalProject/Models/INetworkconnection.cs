﻿using System;
using System.Collections.Generic;
using System.Text;

namespace FinalProject.Models
{
   public interface INetworkconnection
    {
        //interface to define the method used in other part of the code.
        bool IsConnected { get; }
        void checkNetworkConnection();
    }
}
