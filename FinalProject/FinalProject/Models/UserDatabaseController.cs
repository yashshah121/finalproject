﻿using SQLite;
using FinalProject.Models;
using Xamarin.Forms;


namespace FinalProject.Models
{
        public class UserDatabaseController
        {
            static object locker = new object();

            SQLiteConnection db;

            public UserDatabaseController()
            {
                db = DependencyService.Get<ISQLite>().GetConnection();
                db.CreateTable<UserData>();
            }

            public UserData GetUser(string uName)
            {
                lock (locker)
                {
                    return db.Table<UserData>().FirstOrDefault(dbUser => dbUser.Username == uName);
                }
            }

            public void SaveUser(UserData userdata)
            {
                lock (locker)
                {
                    UserData dbUser = db.Table<UserData>().FirstOrDefault(dbu => dbu.Username == userdata.Username);

                    if (dbUser == null)
                    {
                        dbUser = userdata;

                        db.Insert(dbUser);
                    }
                    else
                    {
                        db.Update(userdata);
                    }
                }
            }

            public int DeleteUser(int id)
            {
                lock (locker)
                {
                    return db.Delete<UserData>(id);
                }
            }

    }
}
