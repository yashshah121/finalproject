﻿using SQLite;
using System;
using System.Collections.Generic;
using System.Text;

namespace FinalProject
{
    public class UserData
    {
        [PrimaryKey, AutoIncrement, SQLite.Column("Id")]
        public int Id { get; set ; }
        public string Username { get; set; }
        public string Password { get; set; }
        public UserData() { }
        public UserData(String Username, String Password)
        {
            this.Username = Username;
            this.Password = Password;
        }
        public bool InfoVerify()
        {
            if (!this.Username.Equals("") && !this.Password.Equals(""))
                return true;
            else
                return false;
        }
        public bool CheckInfo()
        {
            if (string.IsNullOrWhiteSpace(Username) && string.IsNullOrWhiteSpace(Password))
                return false;

            return true;
        }
    }
}
