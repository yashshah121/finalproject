﻿using SQLite;
using FinalProject.Models;
using Xamarin.Forms;

namespace FinalProject.Models
{
    public class TokenDatabaseController
    {
        static object locker = new object();

        SQLiteConnection db;

        public TokenDatabaseController()
        {
            db = DependencyService.Get<ISQLite>().GetConnection();
            db.CreateTable<TokenDetails>();
        }

        public TokenDetails GetToken()
        {
            lock (locker)
            {
                if (db.Table<TokenDetails>().Count() == 0)
                {
                    return null;
                }
                else
                {
                    return db.Table<TokenDetails>().First();
                }
            }
        }

        public int SaveToken(TokenDetails Token)
        {
            lock (locker)
            {
                if (Token.Id != 0)
                {
                    db.Update(Token);
                    return Token.Id;
                }
                else
                {
                    return db.Insert(Token);
                }
            }
        }

        public int DeleteToken(int id)
        {
            lock (locker)
            {
                return db.Delete<TokenDetails>(id);
            }
        }
    }
}