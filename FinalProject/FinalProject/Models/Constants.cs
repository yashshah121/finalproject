﻿
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Text;
using Xamarin.Forms;
using Color = System.Drawing.Color;

namespace FinalProject
{
    //constnt data for multiple pages used.
    public class Constants
    {
        public static bool IsDev = true;
        public static Color BackgroundColor = Color.White;
        public static Color MainTextColor = Color.FromArgb(4, 46, 111);
        public static int Login_LogoHeight = 120;
        public static string LoginUrl = "https://test.com/api/Auth/Login";

        public static string NotInternetText = "No internet, please reconnect!";

    }
}