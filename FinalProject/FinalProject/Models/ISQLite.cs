﻿using SQLite;
using System;
using System.Collections.Generic;
using System.Text;

namespace FinalProject.Models
{
    public interface ISQLite
    {
        //interface for defining the method being used in other part of the code
        SQLiteConnection GetConnection();

    }
}
